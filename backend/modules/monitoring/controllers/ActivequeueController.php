<?php

namespace app\modules\monitoring\controllers;

use Yii;
use yii\redis\Connection;

class ActivequeueController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $redis = Yii::$app->redis;
        //$result = $redis->executeCommand('rpush', ['yeah','hello','world']);
        //$result = $redis->executeCommand('KEYS', ['*']);
        $result = $redis->executeCommand('LRANGE', ['hdphdp','0','-1']);

        return $this->render('index',[
            'data' => $result
        ]);
    }

}
