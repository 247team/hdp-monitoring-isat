<?php

namespace app\modules\monitoring;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\monitoring\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
