<?php
use yii\helpers\Html;
$this->title = 'Active Queue';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activequeue-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="grid-view" id="w1">
        <!--<div class="summary">Showing <b>1-9</b> of <b>9</b> items.</div>-->
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Filename</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            foreach ($data as $row) {
                $fields = \yii\helpers\Json::decode($row);
            ?>
            <tr>
                <td><?php echo $no;?></td>
                <td><?php echo $fields['data'];?></td>
            </tr>
            <?php $no++;} ?>
            </tbody>
        </table>
    </div>
</div>