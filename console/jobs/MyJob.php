<?php
namespace console\jobs;

use common\models\QueueLogs;
use common\models\TbFileCollect;
use common\models\TbFileToJob;
use yii\helpers\Json;
use yii\db\Exception;

class MyJob
{
    public function run($job, $data)
    {
        $file = \Yii::getAlias('@rootpath').'\console\input_file\\'.$data;
        $json  = file_get_contents($file, true);
        $start_date = date('Y-m-d H:i:s');
        echo 'Filename: '.$data."\nstart run: ".$start_date;
        $record = Json::decode($json);

        if ($record['table'] == "TB_FILE_TO_JOB") {
            $transaction = TbFileToJob::getDb()->beginTransaction();
        } else if ($record['table'] == "TB_FILE_COLLECT") {
            $transaction = TbFileCollect::getDb()->beginTransaction();
        }

        $total_row = 0;
        $total_success = 0;
        $total_error = 0;
        $message_error = '';
        try {
            $counter = 1;
            foreach ($record['data'] as $row) {
                if ($record['table'] == "TB_FILE_TO_JOB") {
                    $ftj = new TbFileToJob();
                    $rows['TbFileToJob'] = $row;
                } else if ($record['table'] == "TB_FILE_COLLECT") {
                    $ftj = new TbFileCollect();
                    $rows['TbFileCollect'] = $row;
                }
                $ftj->SEQ_ID = date('YmdHis').str_pad($counter, 5, '0', STR_PAD_LEFT);;
                $ftj->load($rows);
                try {
                    $ftj->save();
                    $total_success++;
                } catch(Exception $e) {
                    $error = 1;
                    $message_error .= 'Error code ('.$e->errorInfo[0].') in line '.$counter.'\n';
                    $total_error++;
                }
                $counter++;
                $total_row++;
            }
            if ($total_error == 0) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        //echo $message_error;
        $end_date = date('Y-m-d H:i:s');
        echo "\nend run: ".$end_date."\n\n";

        //save to logs
        $logs = new QueueLogs();
        $logs->FILENAME = $data;
        $logs->TOTAL_ROW = $total_row;
        $logs->TOTAL_SUCCESS = $total_success;
        $logs->TOTAL_ERROR = $total_error;
        $logs->STATUS = ($total_error == 0 ? 'success' : 'error');
        $logs->MESSAGE_ERROR = $message_error;
        $logs->RUN_JOB_DATE = $start_date;
        $logs->END_JOB_DATE = $end_date;
        $logs->save();
    }
} 