<?php

namespace console\controllers;


use yii\console\Controller;

class JobController extends Controller
{
    public function actionQueue($data) {
        \Yii::$app->queue->push('\console\jobs\MyJob', $data,'hdp');
    }

}
