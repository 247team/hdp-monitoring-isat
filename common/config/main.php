<?php
return [
    'timeZone' => 'Asia/Jakarta',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'authManager' => [
				'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\PhpManager'
		],
		'queue' => [
			'class' => 'wh\queue\RedisQueue',
			'redis' => [
				'hostname' => 'localhost',
				'port' => 6379,
				'database' => 0
			]
		],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
    ],
	'modules' => [
	],
    'controllerMap' => [
        'queue' => 'wh\queue\console\controllers\QueueController'
    ],
];
