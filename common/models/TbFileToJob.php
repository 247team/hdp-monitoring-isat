<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tb_file_to_job".
 *
 * @property integer $SEQ_ID
 * @property string $FILENAME
 * @property integer $FILESIZE
 * @property string $FILEDATE
 * @property string $SOURCE_DIR
 * @property integer $ETL_MAP_SEQ_ID
 * @property string $COLL_JOB_ID
 * @property string $JOB_ID
 */
class TbFileToJob extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_file_to_job';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FILENAME', 'FILEDATE', 'SOURCE_DIR', 'ETL_MAP_SEQ_ID', 'COLL_JOB_ID'], 'required'],
            [['FILESIZE', 'ETL_MAP_SEQ_ID'], 'integer'],
            [['FILEDATE'], 'safe'],
            [['FILENAME', 'SOURCE_DIR'], 'string', 'max' => 255],
            [['COLL_JOB_ID', 'JOB_ID'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'SEQ_ID' => 'Seq  ID',
            'FILENAME' => 'Real  Filename',
            'FILESIZE' => 'Filesize',
            'FILEDATE' => 'Filedate',
            'SOURCE_DIR' => 'Source  Dir',
            'ETL_MAP_SEQ_ID' => 'Etl  Map  Seq  ID',
            'COLL_JOB_ID' => 'Coll  Job  ID',
            'JOB_ID' => 'Job  ID',
        ];
    }
}
