<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tb_file_collect".
 *
 * @property integer $SEQ_ID
 * @property string $FILENAME
 * @property string $REAL_FILENAME
 * @property string $SOURCE_DIR
 * @property integer $ETL_MAP_SEQ_ID
 * @property integer $FILESIZE
 * @property string $FILEDATE
 * @property integer $TOTAL_RECORDS
 * @property string $FILE_MODIFY_DATE
 * @property string $COLL_JOB_ID
 * @property string $COLL_START_DATE
 * @property string $COLL_END_DATE
 */
class TbFileCollect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_file_collect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FILENAME', 'REAL_FILENAME', 'SOURCE_DIR', 'ETL_MAP_SEQ_ID', 'FILEDATE', 'COLL_JOB_ID', 'COLL_START_DATE', 'COLL_END_DATE'], 'required'],
            [['ETL_MAP_SEQ_ID', 'FILESIZE', 'TOTAL_RECORDS'], 'integer'],
            [['FILEDATE', 'FILE_MODIFY_DATE', 'COLL_START_DATE', 'COLL_END_DATE'], 'safe'],
            [['FILENAME', 'REAL_FILENAME', 'SOURCE_DIR'], 'string', 'max' => 255],
            [['COLL_JOB_ID'], 'string', 'max' => 50],
            [['REAL_FILENAME'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'SEQ_ID' => 'Seq  ID',
            'FILENAME' => 'Filename',
            'REAL_FILENAME' => 'Real  Filename',
            'SOURCE_DIR' => 'Source  Dir',
            'ETL_MAP_SEQ_ID' => 'Etl  Map  Seq  ID',
            'FILESIZE' => 'Filesize',
            'FILEDATE' => 'Filedate',
            'TOTAL_RECORDS' => 'Total  Records',
            'FILE_MODIFY_DATE' => 'File  Modify  Date',
            'COLL_JOB_ID' => 'Coll  Job  ID',
            'COLL_START_DATE' => 'Coll  Start  Date',
            'COLL_END_DATE' => 'Coll  End  Date',
        ];
    }
}
