<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "queue_logs".
 *
 * @property integer $QL_ID
 * @property string $FILENAME
 * @property integer $TOTAL_ROW
 * @property integer $TOTAL_SUCCESS
 * @property integer $TOTAL_ERROR
 * @property string $STATUS
 * @property string $MESSAGE_ERROR
 */
class QueueLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'queue_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FILENAME', 'TOTAL_ROW', 'TOTAL_SUCCESS', 'TOTAL_ERROR', 'STATUS'], 'required'],
            [['TOTAL_ROW', 'TOTAL_SUCCESS', 'TOTAL_ERROR'], 'integer'],
            [['MESSAGE_ERROR'], 'string'],
            [['FILENAME'], 'string', 'max' => 500],
            [['STATUS'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'QL_ID' => 'Ql  ID',
            'FILENAME' => 'Filename',
            'TOTAL_ROW' => 'Total  Row',
            'TOTAL_SUCCESS' => 'Total  Success',
            'TOTAL_ERROR' => 'Total  Error',
            'STATUS' => 'Status',
            'MESSAGE_ERROR' => 'Message  Error',
        ];
    }
}
